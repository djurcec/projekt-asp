Projekt iz Algoritama i struktura podataka.

1. Opis
    Projekt je napravlje u Visual Studio 22. Ovaj projekt koristi vector i map za spremanje podataka. Pri pokretanju program čita preko 500000 linija iz FrautTest.csv filea te ih sprema u tri mape, svaka mapa je jedan stupac u .csv datoteci. Za ovaj projekt su odabrani stupci cc_num, amt i unix_time. Svaki od tih podataka se spremaju u svoju mapu koja sadrži ključ. Ključ unutar mape je broj redka unutar .csv datoteke. Nad svakom od tih mapa postoje funkcije za: pretraživanje po vrijednosti ili ključu, brisanje po ključu ili vrijednosti, dodavanje po ključu i vrijednosti, traženje najvećih i najmanjih vrijednosti. Svaka funkcija može obrađivati jedan ili n podataka, ovisno o pozivu u mainu. Ako se u mainu pozove funkcija i stavi se parametar 1, funkcija će obaditi samo jedan podatak.

2. Rezultati ispisa
    Za unos podataka: 49275 ms
    Vrijeme za ispis podataka po određenom ključu: 0 ms
    Vrijeme za pronalazak cc_num podataka po kljucu (dva podatka): 5119 ms
    Vrijeme za pronalazak amt podataka po vrijednosti (dva podatka): 5241
    Vrijeme za brisanje po kljucu (dva podatka): 3420 ms
    Vrijeme da se obriše cc_num podatka po vrijednosti: 3036 ms
    Dohvat najvećih cc_num podataka (pet podataka): 748 ms
    Dohvat najmanjih cc_num podataka (dva podatka): 747 ms
    Dodavanje amt po vrijednosti (dva podatka): 3262 ms 

3. Korišteni Library
    iostream
    fstream
    sstream
    chrono
    map 
    string
    algorithm
    vector

4. Zaključak
    Većina ovih funkcija obradi podatke i brže, ali čeka input od korisnika koji zna biti sporiji. Mape su jako zahvalne jer je u njima jako lagano pronalaziti, brisati i dodavati vrijednosti, kada znamo ključ. Kada ne znamo ključ može proći dosta vremena da se ta vrijednost pronađe u mapi.
