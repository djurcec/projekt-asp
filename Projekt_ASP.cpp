#include <iostream>
#include <fstream>
#include <sstream>
#include <chrono>
#include <map>
#include <string>
#include <algorithm>
#include <vector>

using namespace std;
using namespace chrono;

bool provjeri(string a) {
    
        bool result=true;
        try
        {

            int n = stoi(a);
        }
        catch (const invalid_argument& e)
        {
            result = false;
        }
        catch (const out_of_range& e)
        {
            result = false;
        }
        return result;
}

bool cmp(pair<int, unsigned long long int>& a,
    pair<int, unsigned long long int>& b)
{
    return a.second > b.second;
}

bool cmp1(pair<int, float>& a,
    pair<int, float>& b)
{
    return a.second > b.second;
}

class Podaci {
public:
    void unosPodatakaIzCSV() {
        fstream fin;
        fin.open("C:/Users/Korisnik/Downloads/fraudTest.csv", ios::in);
        string line;
        int a = 0;
        getline(fin, line);
        while (getline(fin, line)) {
            if (a >= 1) {
                stringstream ss(line);
                string cc_num;
                string d;
                string amt;
                string unix_time;
                getline(ss, d, ',');
                getline(ss, d, ',');
                getline(ss, cc_num, ',');
                getline(ss, d, ',');
                getline(ss, d, ',');
                if (d.find('0') != string::npos || d.find('1') != string::npos || d.find('2') != string::npos || d.find('3') != string::npos || d.find('4') != string::npos || d.find('5') != string::npos || d.find('6') != string::npos || d.find('7') != string::npos || d.find('8') != string::npos || d.find('9') != string::npos) {
                    amt = d;
                }
                else if (d.find('a') != string::npos || d.find('e') != string::npos || d.find('i') != string::npos || d.find('o') != string::npos || d.find('u') != string::npos) {
                    getline(ss, amt, ',');
                    if (amt.find('a') != string::npos || amt.find('e') != string::npos || amt.find('i') != string::npos || amt.find('o') != string::npos || amt.find('u') != string::npos) {
                        getline(ss, amt, ',');
                    }
                }
                getline(ss, d, ',');
                getline(ss, d, ',');
                getline(ss, d, ',');
                getline(ss, d, ',');
                getline(ss, d, ',');
                getline(ss, d, ',');
                getline(ss, d, ',');
                getline(ss, d, ',');
                getline(ss, d, ',');
                getline(ss, d, ',');
                getline(ss, d, ',');
                getline(ss, d, ',');
                getline(ss, d, ',');
                getline(ss, unix_time, ',');
                while (provjeri(unix_time) == false) {
                    getline(ss, unix_time, ',');
                }
                if (unix_time.find('a') != string::npos || unix_time.find('e') != string::npos || unix_time.find('i') != string::npos || unix_time.find('o') != string::npos || unix_time.find('u') != string::npos || unix_time.find('b') != string::npos || unix_time.find('c') != string::npos || unix_time.find('d') != string::npos) {
                    getline(ss, unix_time, ',');
                }
                cc_numv[a - 1] = stoll(cc_num);
                amtv[a - 1] = stof(amt);
                unix_timev[a - 1] = stol(unix_time);
            }
            a++;
        }
    }

    void pronadiCc_numvPoKljucu(int b) {
        int kljuc;
        for (int i = 0; i < b; i++) {
            cout << "Unesi kljuc:" << endl;
            cin >> kljuc;
            auto it = cc_numv.find(kljuc);
            if (it != cc_numv.end()) {
                cout << "Element s tim kljucem je pronaden" << endl;
            }
        }
    }
    void pronadiAmtvPoKljucu(int b) {
        int kljuc;
        for (int i = 0; i < b; i++) {
            cout << "Unesi kljuc: " << endl;
            cin >> kljuc;
            auto it = amtv.find(kljuc);
            if (it != amtv.end()) {
                cout << "Element s tim kljucem je pronaden" << endl;
            }
        }
    }
    void pronadiUnix_timevPoKljucu(int b) {
        int kljuc;
        for (int i = 0; i < b; i++) {
            cout << "Unesi kljuc: " << endl;
            cin >> kljuc;
            auto it = unix_timev.find(kljuc);
            if (it != unix_timev.end()) {
                cout << "Element s tim kljucem je pronaden" << endl;
            }
        }
    }

    void pronadiCc_numvPoVrijednosti(int b) {
        unsigned long long int vrijednost;
        int a;
        for (int i = 0; i < b; i++) {
            a = 0;
            cout << "Unesi vrijednost:" << endl;
            cin >> vrijednost;
            for (auto it = cc_numv.begin(); it != cc_numv.end(); it++) {
                if (vrijednost == it->second) {
                    a++;
                }
            }
            cout << "Pronadeno je: " << a << " vrijednosti" << endl;
        }

    }
    void pronadiAmtvPoVrijednosti(int b) {
        float vrijednost;
        int a;
        for (int i = 0; i < b; i++) {
            a = 0;
            cout << "Unesi vrijednost:" << endl;
            cin >> vrijednost;
            for (auto it = amtv.begin(); it != amtv.end(); it++) {
                if (vrijednost == it->second) {
                    a++;
                }
            }
            cout << "Pronadeno je: " << a << " vrijednosti" << endl;
        }
    }
    void pronadiUnix_timevPoVrijednosti(int b) {
        unsigned long long int vrijednost;
        int a;
        for (int i = 0; i < b; i++) {
            a = 0;
            cout << "Unesi vrijednost:" << endl;
            cin >> vrijednost;
            for (auto it = unix_timev.begin(); it != unix_timev.end(); it++) {
                if (vrijednost == it->second) {
                    a++;
                }
            }
            cout << "Pronadeno je: " << a << " vrijednosti" << endl;
        }
    }

    void brisanjePoKljucu(int b) {
        int kljuc;
        for (int i = 0; i < b; i++) {
            cout << "Unesi kljuc: " << endl;
            cin >> kljuc;
            auto it = cc_numv.find(kljuc);
            cc_numv.erase(it);
            auto it1 = amtv.find(kljuc);
            amtv.erase(it1);
            auto it2 = unix_timev.find(kljuc);
            unix_timev.erase(it2);
        }
    }
    void brisanjeCc_numvPoVrijednosti(int b) {
        unsigned long long int vrijednost;
        for (int i = 0; i < b; i++) {
            cout << "Unesi vrijednost: " << endl;
            cin >> vrijednost;
            for (auto it = cc_numv.begin(); it != cc_numv.end(); it++) {
                if (it->second == vrijednost) {
                    cc_numv.erase(it);
                    int kljuc = it->first;
                    amtv.erase(kljuc);
                    unix_timev.erase(kljuc);
                }
            }
        }
    }
    void brisanjeAmtvPoVrijednosti(int b) {
        float vrijednost;
        for (int i = 0; i < b; i++) {
            cout << "Unesi vrijednost: " << endl;
            cin >> vrijednost;
            for (auto it = amtv.begin(); it != amtv.end(); it++) {
                if (it->second == vrijednost) {
                    amtv.erase(it);
                    int kljuc = it->first;
                    cc_numv.erase(kljuc);
                    unix_timev.erase(kljuc);
                }
            }
        }
    }
    void brisanjeUnix_timevPoVrijednosti(int b) {
        unsigned long long int vrijednost;
        for (int i = 0; i < b; i++) {
            cout << "Unesi vrijednost: " << endl;
            cin >> vrijednost;
            for (auto it = unix_timev.begin(); it != unix_timev.end(); it++) {
                if (it->second == vrijednost) {
                    unix_timev.erase(it);
                    int kljuc = it->first;
                    amtv.erase(kljuc);
                    cc_numv.erase(kljuc);
                }
            }
        }
    }

    void dohvatiNajveciCc_num(int b) {
        vector<pair<int, unsigned long long int>> vec;
        map<int, unsigned long long  int> ::iterator it2;
        for (it2 = cc_numv.begin(); it2 != cc_numv.end(); it2++)
        {
            vec.push_back(make_pair(it2->first,it2->second));
        }
        sort(vec.begin(), vec.end(), cmp);
        auto it = vec.begin();
        for (int i = 0; i < b; i++) {
            cout << it->second << endl;
            it++;
        }
    }
    void dohvatiNajveciAmt(int b) {
        vector<pair<int, float>> vec;
        map<int, float> ::iterator it2;
        for (it2 = amtv.begin(); it2 != amtv.end(); it2++)
        {
            vec.push_back(make_pair(it2->first, it2->second));
        }
        sort(vec.begin(), vec.end(), cmp1);
        auto it = vec.begin();
        for (int i = 0; i < b; i++) {
            cout << it->second << endl;
            it++;
        }
    }
    void dohvatiNajveciUnix_time(int b) {
        vector<pair<int, unsigned long long int>> vec;
        map<int, unsigned long long  int> ::iterator it2;
        for (it2 = unix_timev.begin(); it2 != unix_timev.end(); it2++)
        {
            vec.push_back(make_pair(it2->first, it2->second));
        }
        sort(vec.begin(), vec.end(), cmp);
        auto it = vec.begin();
        for (int i = 0; i < b; i++) {
            cout << it->second << endl;
            it++;
        }
    }
    void dohvatiNajmanjiCc_num(int b) {
        vector<pair<int, unsigned long long int>> vec;
        map<int, unsigned long long  int> ::iterator it2;
        for (it2 = cc_numv.begin(); it2 != cc_numv.end(); it2++)
        {
            vec.push_back(make_pair(it2->first, it2->second));
        }
        sort(vec.begin(), vec.end(), cmp);
        reverse(vec.begin(), vec.end());
        auto it = vec.begin();
        for (int i = 0; i < b; i++) {
            cout << it->second << endl;
            it++;
        }
    }
    void dohvatiNajmanjiAmt(int b) {
        vector<pair<int, float>> vec;
        map<int, float> ::iterator it2;
        for (it2 = amtv.begin(); it2 != amtv.end(); it2++)
        {
            vec.push_back(make_pair(it2->first, it2->second));
        }
        sort(vec.begin(), vec.end(), cmp1);
        reverse(vec.begin(), vec.end());
        auto it = vec.begin();
        for (int i = 0; i < b; i++) {
            cout << it->second << endl;
            it++;
        }
    }
    void dohvatiNajmanjiUnix_time(int b) {
        vector<pair<int, unsigned long long int>> vec;
        map<int, unsigned long long  int> ::iterator it2;
        for (it2 = unix_timev.begin(); it2 != unix_timev.end(); it2++)
        {
            vec.push_back(make_pair(it2->first, it2->second));
        }
        sort(vec.begin(), vec.end(), cmp);
        reverse(vec.begin(), vec.end());
        auto it = vec.begin();
        for (int i = 0; i < b; i++) {
            cout << it->second << endl;
            it++;
        }
    }
    bool PostojiKljuc(int kljuc) {
        auto it = cc_numv.find(kljuc);
        auto it2 = amtv.find(kljuc);
        auto it3 = unix_timev.find(kljuc);
        if (it != cc_numv.end() && it2 != amtv.end() && it3 != unix_timev.end()) {
            return true;
        }
        return false;
    }
    void Ispis(int kljuc) {
        if (PostojiKljuc(kljuc)) {
            cout << "Podatak u cc_num mapi na tom kljucu: " << cc_numv[kljuc] << endl;
            cout << "Podatak u amt mapi na tom kljucu: " << amtv[kljuc] << endl;
            cout << "Podatak u unix_time mapi na tom kljucu: " << unix_timev[kljuc] << endl;
        }
    }

    void dodajCc_numPoVrijednosti(int b) {
        int c = cc_numv.size();
        for (int a = 1; a <= b; a++) {
            unsigned long long int vrijednost;
            cout << "Unesi vrijednost: " << endl;
            cin >> vrijednost;
            cc_numv[c + a] = vrijednost;
    }
    }
    void dodajAmtPoVrijednosti(int b){
        int c = amtv.size();
        for (int a = 1; a <= b; a++) {
            float vrijednost;
            cout << "Unesi vrijednost: " << endl;
            cin >> vrijednost;
            amtv[c + a] = vrijednost;
        }
    }
    void dodajUnix_timePoVrijednosti(int b){
        int c = unix_timev.size();
        for (int a = 1; a <= b; a++) {
            unsigned long long int vrijednost;
            cout << "Unesi vrijednost: " << endl;
            cin >> vrijednost;
            unix_timev[c + a] = vrijednost;
        }
    }
private:
    map<int ,unsigned long long int> cc_numv;
    map<int, float> amtv;
    map<int, unsigned  long long int> unix_timev;
};


int main()
{   
    Podaci p;
    cout << "Unos podataka: " << endl;
    auto start = high_resolution_clock::now();
    p.unosPodatakaIzCSV();
    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<milliseconds>(stop - start);
    cout << "Vrijeme potrebno da se procitaju i unesu svi podaci iz csv filea: " << duration.count() << endl;
    cout << "Ispis podataka: " << endl;
    start = high_resolution_clock::now();
    p.Ispis(515000);
    stop = high_resolution_clock::now();
    duration = duration_cast<milliseconds>(stop - start);
    cout << "Vrijeme potrebno da se dohvate i ispisu svi podaci na odredenom kljucu: " << duration.count() << endl;
    cout << "PronadiCc_numvPoKjucu: " << endl;
    start = high_resolution_clock::now();
    p.pronadiCc_numvPoKljucu(2);
    stop = high_resolution_clock::now();
    duration = duration_cast<milliseconds>(stop - start);
    cout << "Vrijeme potrebno da se pronadu cc_num podaci na odredenom kljucu: " << duration.count() << endl;
    cout << "PronadiAmtvPoVrijednosti: " << endl;
    start = high_resolution_clock::now();
    p.pronadiAmtvPoVrijednosti(2);
    stop = high_resolution_clock::now();
    duration = duration_cast<milliseconds>(stop - start);
    cout << "Vrijeme potrebno da se pronadu atm podaci koji imaju odredenu vrijednost: " << duration.count() << endl;
    cout << "BrisanjePoKljucu: " << endl;
    start = high_resolution_clock::now();
    p.brisanjePoKljucu(2);
    stop = high_resolution_clock::now();
    duration = duration_cast<milliseconds>(stop - start);
    cout << "Vrijeme potrebno da se obrisu svi podaci na nekom kljucu: " << duration.count() << endl;
    cout << "BrisanjeUnix_timevPoVrijednosti: " << endl;
    start = high_resolution_clock::now();
    p.brisanjeUnix_timevPoVrijednosti(1);
    stop = high_resolution_clock::now();
    duration = duration_cast<milliseconds>(stop - start);
    cout << "Vrijeme potrebno da se obrisu svi podaci koji imaju isti kljuc kao i cc_num vrijednost koja je unesena: " << duration.count() << endl;
    cout << "DohvatiNajveciCc_num: " << endl;
    start = high_resolution_clock::now();
    p.dohvatiNajveciCc_num(5);
    stop = high_resolution_clock::now();
    duration = duration_cast<milliseconds>(stop - start);
    cout << "Vrijeme potrebno da se pronadu cc_num podaci s najvecom vrijednosti: " << duration.count() << endl;
    cout << "DohvatiNajmanjiCc_num: " << endl;
    start = high_resolution_clock::now();
    p.dohvatiNajmanjiCc_num(2);
    stop = high_resolution_clock::now();
    duration = duration_cast<milliseconds>(stop - start);
    cout << "Vrijeme potrebno da se pronadu cc_num podaci s najmanjom vrijednost: " << duration.count() << endl;
    
    cout << "DodajAmtPoVrijednosti: " << endl;
    start = high_resolution_clock::now();
    p.dodajAmtPoVrijednosti(2);
    stop = high_resolution_clock::now();
    duration = duration_cast<milliseconds>(stop - start);
    cout << "Vrijeme potrebno da se dodaju atm podaci koji imaju odredenu vrijednost: " << duration.count() << endl;
}
